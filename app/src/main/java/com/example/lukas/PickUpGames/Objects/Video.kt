package com.example.lukas.PickUpGames.Objects

data class Video(
        var name : String,
        var video_id : String
)