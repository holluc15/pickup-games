package com.example.lukas.PickUpGames.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import com.example.lukas.PickUpGames.R
import kotlinx.android.synthetic.main.web_activity.*
import android.webkit.WebViewClient

/**
 * Created by Lukas on 01.08.2018.
 */

class MyWebFragment : Fragment() {

    private var url : String = ""
    private var steam : Boolean = false

    companion object {
        fun newInstance(url: String,steam : Boolean) = MyWebFragment().apply {
            this.url = url
            this.steam = steam

        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.web_activity,container,false)

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val address : String
        web_loading_layout.visibility = View.VISIBLE
        if(steam) {
            address = "https://store.steampowered.com/app/$url"
        }else{
            address = url
        }
        web_view.settings.javaScriptEnabled = true
        web_view.settings.pluginState = WebSettings.PluginState.ON
        web_view.loadUrl(address)
        web_view.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                if(web_loading_layout != null) {
                    web_loading_layout.visibility = View.GONE
                }

            }

        }

        super.onViewCreated(view, savedInstanceState)
    }

}