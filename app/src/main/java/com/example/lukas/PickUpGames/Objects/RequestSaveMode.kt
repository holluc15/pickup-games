package com.example.lukas.PickUpGames.Objects

data class RequestSaveMode(
        var success : Boolean,
        var code : Int,
        var message : String?,
        var status : Boolean


)