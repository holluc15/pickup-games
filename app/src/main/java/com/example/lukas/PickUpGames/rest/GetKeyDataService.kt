package com.example.lukas.PickUpGames.rest

import com.example.lukas.PickUpGames.Objects.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query



interface GetKeyDataService {

    @GET("getApiKey.php")
    fun getAPIKey(@Query(value = "auth") auth: String): Observable<Response<ApiKeyResponse>>

    @GET("updateRequests.php?auth=p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww")
    fun updateRequests(@Query(value = "api_key") api_key: String) : Observable<Response<ApiKeyResponse>>

    @GET("updateLikes.php?auth=p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww")
    fun updateLikes(@Query(value = "game_id") game_id: Int,@Query(value = "dislike") dislike: Boolean): Observable<Response<ApiKeyResponse>>

    @GET("getRequestSaveMode.php?auth=p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww")
    fun getRequestSaveMode(): Observable<Response<RequestSaveMode>>

    @GET("getTopGames.php?auth=p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww")
    fun getTopGames(): Observable<Response<TopGamesResult>>

    @GET("getOurFavorites.php?auth=p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww")
    fun getOurFavourites() : Observable<Response<OurFavourites>>






}