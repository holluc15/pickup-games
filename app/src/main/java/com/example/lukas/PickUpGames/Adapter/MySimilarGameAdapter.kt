package com.example.lukas.PickUpGames.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Fragments.MyGameFragment
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.Objects.Game
import com.example.lukas.PickUpGames.R
import com.example.lukas.PickUpGames.replaceFragmentSafely
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.similar_game_item.view.*


class MySimilarGameAdapter(var myDataset: MutableList<Game>? = mutableListOf(), var context : Context?,var openedBy : String) : RecyclerView.Adapter<MySimilarGameAdapter.ViewHolder>() {

    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.similar_game_item, parent, false) as RelativeLayout

        return ViewHolder(cont).listen { pos, type ->
            val item = myDataset?.get(pos)

            val fragment = MyGameFragment.newInstance(item!!,R.drawable.gradient_1,openedBy)
            (context as MainActivity).replaceFragmentSafely(fragment,context!!.getString(R.string.game_fragment),true, R.id.frame)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        myDataset?.let {
            val item =  myDataset!![position]

            val cover = item.cover
            Picasso.get().load("http:${cover?.url}").placeholder(R.drawable.ic_help_outline_black_24dp).error(R.drawable.ic_cancel).into(holder.layout.similar_game_icon)

            holder.layout.similar_game_name.text = item.name


        }
    }

    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }




}