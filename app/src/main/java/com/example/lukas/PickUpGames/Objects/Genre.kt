package com.example.lukas.PickUpGames.Objects

data class Genre(
        var id : Int,
        var name : String
)