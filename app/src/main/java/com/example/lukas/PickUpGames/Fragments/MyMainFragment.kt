package com.example.lukas.PickUpGames.Fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.Adapter.MyMainAdapter
import com.example.lukas.PickUpGames.Objects.Category
import kotlinx.android.synthetic.main.main_screen.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Objects.CachedData
import com.example.lukas.PickUpGames.Objects.Game
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Lukas on 01.08.2018.
 */

class MyMainFragment : Fragment() {
    private var mainAdapter: MyMainAdapter?= null
    private var apiService = APIKeyServiceSDK.service
    private var dataService = DataServiceSDK
    private var genreString = ""
    private var platformString = ""
    private lateinit var loadingFragment : MyLoadingFragment
    private val auth = "p67BM2TZq5rmaD5VC99GGQwuBnwfUct673UBqa5hGRT7HUew6SQegBusrwPDjauGPneKbNnKNyUP52yQBwKgw7rEDddGuAH2T4ww"
    private var backupKey = "6a3abbf94630c395b86f430c040ce36d"
    private var fragment : MyFilterFragment ?= null


    companion object {
        fun newInstance(fragment: MyFilterFragment) = MyMainFragment().apply {
          this.fragment = fragment
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        if((context as MainActivity).isNetworkAvailable()) {
            getRequestSaveMode()
        }

        if((context as MainActivity).isNetworkAvailable() && fragment != null) {


            apiService.getAPIKey(auth).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->

                if (response.isSuccessful) {
                    var item = response.body()
                    if (item?.apikey != null) {
                        dataService.apiKey = item.apikey!!
                        loadingFragment = MyLoadingFragment()
                        (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, loadingFragment).addToBackStack("LoadingFragment").commitAllowingStateLoss()
                        loadData()

                    }

                } else {
                    dataService.apiKey = backupKey
                    loadingFragment = MyLoadingFragment()
                    (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, loadingFragment).addToBackStack("LoadingFragment").commitAllowingStateLoss()
                    loadData()
                }
            }


        } else if(!(context as MainActivity).isNetworkAvailable()){
            var fragment = MyNoConnectionFragment()
            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack("NoConnectionFragment").commitAllowingStateLoss()
        }
        var v = inflater.inflate(R.layout.main_screen,container,false)
        return v

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mainAdapter = MyMainAdapter(context = context)
        main_list.adapter = mainAdapter

        addListener()


        //main_list.layoutManager = GridLayoutManager(context,rows,GridLayoutManager.HORIZONTAL,false)
        main_list.layoutManager = LinearLayoutManager(context)
        addCategories()


        val keyboard = (context as MainActivity).currentFocus
        if (keyboard != null) {
            val imm = (context as MainActivity).getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(keyboard.windowToken, 0)
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun getRequestSaveMode() {
        apiService.getRequestSaveMode().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
            if(it.isSuccessful) {
                var item = it.body()
                CachedData.requestSaveMode = item!!.status
            }
        }

    }



    private fun addListener() {
        search_view.setOnClickListener{
            (context as MainActivity).replaceFragmentSafely(MySearchFragment(),"SearchFragment",true,R.id.frame)
        }

        search_button_mid.setOnClickListener{
            (context as MainActivity).replaceFragmentSafely(MySearchFragment(),"SearchFragment",true,R.id.frame)
        }

        stored_games_button.setOnClickListener {
            (context as MainActivity).replaceFragmentSafely(MySavedGamesFragment(),"SavedGamesFragment",true,R.id.frame)
        }

        filter_button.setOnClickListener{
            (context as MainActivity).replaceFragmentSafely(MyFilterFragment(),"SavedGamesFragment",true,R.id.frame)
        }
    }


    private fun addCategories() {
        var list = mutableListOf<Category>()
        for (i in 1..5) {
            list.add(Category("Category $i",false,0))
        }
        mainAdapter?.myDataset = list
        mainAdapter?.notifyDataSetChanged()
    }

    private fun getAllPlatforms() {
        if(platformString.isNotEmpty()) {
            dataService.service.getAllPlatforms(platformString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
                if (!response.isSuccessful) {
                    var fragment = MySomethingWentWrongFragment()
                    (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack("SomethingWentWrongFragment").commitAllowingStateLoss()
                }else{
                    var item = response.body()
                    CachedData.platforms.addAll(item!!)
                    loadingFragment.setProgress(100)
                    if(fragment == null) {
                        var fragment = MyMainFragment()
                        (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack("MainFragment").commitAllowingStateLoss()
                    }else{
                        loadingFragment.setProgress(100)
                        var fragment = MyFilteredGamesFragment.newInstance(CachedData.games)
                        (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack("FilteredGamesFragment").commitAllowingStateLoss()
                    }


                }
            }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
        }else if(fragment != null) {
            loadingFragment.setProgress(100)
            var fragment = MyFilteredGamesFragment.newInstance(CachedData.games)
            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack("FilteredGamesFragment").commitAllowingStateLoss()
        }

    }

    private fun loadData() {

        var map : Map<String,String>
        map = mutableMapOf()
        if(fragment != null) {
            map = fragment!!.getOptionMap()
        }else{
            map["filter[rating][gte]"] = "90"
        }

        if(map.isNotEmpty()) {
        dataService.service.getGames(map).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                var fragment = MySomethingWentWrongFragment()
                (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack("SomethingWentWrongFragment").commitAllowingStateLoss()
            }else{
                var filteredItems = mutableListOf<Game>()
                val items = response.body()
                items?.forEach {
                    if(it.cover != null && it.platforms != null) {
                        filteredItems.add(it)
                    }

                }

                CachedData.games = filteredItems
                var genres : MutableList<Int> = mutableListOf()
                var platforms : MutableList<Int> = mutableListOf()
                items?.forEach{
                    if(it.genres != null) {
                        genres.addAll(it.genres!!)
                    }

                    if(it.platforms != null) {
                        platforms.addAll(it.platforms!!)
                    }

                }



                genreString = (context as MainActivity).createIDString(genres)
                platformString = (context as MainActivity).createIDString(platforms)
                loadingFragment.setProgress(25)
                getAllGenres()
                getAllPlatforms()
            }
            }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
        }
    }

    private fun getAllGenres() {
        if(genreString.isNotEmpty()) {
            dataService.service.getAllGenres(genreString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
                if (!response.isSuccessful) {
                    var fragment = MySomethingWentWrongFragment()
                    (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack("SomethingWentWrongFragment").commitAllowingStateLoss()
                } else {
                    val item = response.body()
                    CachedData.genres.addAll(item!!)
                    loadingFragment.setProgress(66)
                }
            }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
        }
    }




}