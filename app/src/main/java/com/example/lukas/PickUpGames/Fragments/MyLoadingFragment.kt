package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.R
import kotlinx.android.synthetic.main.loading_activity.*


/**
 * Created by Lukas on 01.08.2018.
 */

class MyLoadingFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.loading_activity,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loading_bar.isEnabled = false
        loading_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
               if(p1 == (100)) {
                   (context as MainActivity).supportFragmentManager.popBackStack()
               }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        }
        )
        super.onViewCreated(view, savedInstanceState)
    }

    fun setProgress(progress : Int) {
        if(loading_bar != null) {
            loading_bar.progress = progress
        }
    }


}