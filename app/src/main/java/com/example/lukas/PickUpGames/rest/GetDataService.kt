package com.example.lukas.PickUpGames.rest

import com.example.lukas.PickUpGames.Objects.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap


interface GetDataService {

    @GET("games/?fields=*&limit=50")
    fun getGames(@QueryMap options: Map<String, String>): Observable<Response<MutableList<Game>>>

    @GET("games/{ids}/?fields=*&limit=50")
    fun getGamesById(@Path(value = "ids") games_id: String): Observable<Response<MutableList<Game>>>

    @GET("platforms/{ids}/?fields=name,logo")
    fun getAllPlatforms(@Path(value = "ids") platform_ids: String): Observable<Response<MutableList<Platform>>>

    @GET("genres/{ids}/?fields=name")
    fun getAllGenres(@Path(value = "ids") genres_id: String): Observable<Response<MutableList<Genre>>>

    @GET("games/?fields=*&limit=50")
    fun searchForGame(@QueryMap options: Map<String, String>): Observable<Response<MutableList<Game>>>

    @GET("release_dates/?limit=15")
    fun getComingSoonGames(@Query("filter[date][gt]") id: Long,@Query("fields") fields: String): Observable<Response<MutableList<GameID>>>









}