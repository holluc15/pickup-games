package com.example.lukas.PickUpGames.rest

import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import com.google.gson.GsonBuilder
import retrofit2.converter.gson.GsonConverterFactory


object APIKeyServiceSDK {

    var service: GetKeyDataService
    val baseUrl = "https://pickupgames.tk/api/"
    private var httpclient: OkHttpClient


    init {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

        val builder = Retrofit.Builder()
        val gson = GsonBuilder()
                .setLenient()
                .create()
        builder.baseUrl(baseUrl)
        builder.addConverterFactory(ScalarsConverterFactory.create())
        builder.addConverterFactory(MoshiConverterFactory.create(moshi))
        builder.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        builder.addConverterFactory(GsonConverterFactory.create(gson))

        val okBuilder = OkHttpClient.Builder()

        okBuilder.addInterceptor { chain ->

            val original = chain.request()

            val request = original.newBuilder()
                    //.header("Content-Type", "application/json;charset=utf8")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build()
            chain.proceed(request)

        }


        okBuilder.addInterceptor(logging)
        okBuilder.connectTimeout(50, TimeUnit.SECONDS)
        okBuilder.readTimeout(50, TimeUnit.SECONDS)


        httpclient = okBuilder.build()
        builder.client(httpclient)



        val retrofit = builder.build()

        service = retrofit.create<GetKeyDataService>(GetKeyDataService::class.java)

    }

}