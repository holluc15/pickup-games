package com.example.lukas.PickUpGames.Objects

data class LikedGames(
        var game_id : Int,
        var likes : Int
)