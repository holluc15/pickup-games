package com.example.lukas.PickUpGames.Objects

data class Category (
        var text : String,
        var expanded : Boolean,
        var itemColor : Int
)