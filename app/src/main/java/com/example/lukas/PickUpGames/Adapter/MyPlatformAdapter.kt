package com.example.lukas.PickUpGames.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Objects.Platform
import com.example.lukas.PickUpGames.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.platform_item.view.*


class MyPlatformAdapter(var myDataset: MutableList<Platform>? = mutableListOf(), var context : Context?) : RecyclerView.Adapter<MyPlatformAdapter.ViewHolder>() {

    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.platform_item, parent, false) as RelativeLayout

        return ViewHolder(cont)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        myDataset?.let {
            val item =  myDataset!![position]
            holder.layout.game_name.text = item.name
            if(item.name.contains("Android")) {
                holder.layout.platform_icon.setImageResource(R.drawable.android)
            }else if(item.name.contains("PlayStation")){
                holder.layout.platform_icon.setImageResource(R.drawable.playstation)
            }else if(item.name.contains("Linux")){
                holder.layout.platform_icon.setImageResource(R.drawable.linux)
            }else if(item.name.contains("Xbox")){
                holder.layout.platform_icon.setImageResource(R.drawable.xbox)
            }else if(item.name.contains("iOS")){
                holder.layout.platform_icon.setImageResource(R.drawable.ios)
            }else if(item.name.contains("PC")){
                holder.layout.platform_icon.setImageResource(R.drawable.windows)
            }else{
                val logo = item.logo?.url
                Picasso.get().load("http:$logo").placeholder(R.drawable.ic_computer_black_24dp).error(R.drawable.ic_help_outline_black_24dp).into(holder.layout.platform_icon)
            }


        }
    }

    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }



}

