package com.example.lukas.PickUpGames

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.lukas.PickUpGames.Fragments.MyMainFragment
import com.example.lukas.PickUpGames.Objects.CachedData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.example.lukas.PickUpGames.Fragments.MyLoadingFragment
import com.example.lukas.PickUpGames.Fragments.MyNoConnectionFragment
import com.example.lukas.PickUpGames.Fragments.MySomethingWentWrongFragment
import com.example.lukas.PickUpGames.Objects.Genre
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK


class MainActivity : AppCompatActivity() {

    private var apiService = APIKeyServiceSDK.service
    private var dataService = DataServiceSDK
    private var genreString = ""
    private var platformString = ""
    private lateinit var loadingFragment : MyLoadingFragment
    private var auth = ""
    private var backupKey = ""
    var apiKey : String = ""

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount > 2) {
            super.onBackPressed()
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        auth = this.getString(R.string.auth)
        backupKey = this.getString(R.string.backup_key)
        if(isNetworkAvailable()) {

                apiService.getAPIKey(auth).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->

                    if (response.isSuccessful) {
                        val item = response.body()
                        if (item?.apikey != null) {
                            apiKey = item.apikey!!
                            dataService.apiKey = item.apikey!!
                            loadingFragment = MyLoadingFragment()
                            supportFragmentManager.beginTransaction().add(R.id.frame, loadingFragment).addToBackStack(this.getString(R.string.loading_fragment)).commitAllowingStateLoss()
                            loadData()
                        } else {
                            dataService.apiKey = backupKey
                            loadingFragment = MyLoadingFragment()
                            supportFragmentManager.beginTransaction().add(R.id.frame, loadingFragment).addToBackStack(this.getString(R.string.loading_fragment)).commitAllowingStateLoss()
                            loadData()
                        }

                    } else {
                        dataService.apiKey = backupKey
                        loadingFragment = MyLoadingFragment()
                        supportFragmentManager.beginTransaction().add(R.id.frame, loadingFragment).addToBackStack(this.getString(R.string.loading_fragment)).commitAllowingStateLoss()
                        loadData()
                    }


                }


            } else {
                val fragment = MyNoConnectionFragment()
                supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack(this.getString(R.string.no_connection_fragment)).commitAllowingStateLoss()
            }

        }

    private fun getAllPlatforms() {
        dataService.service.getAllPlatforms(platformString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                val fragment = MySomethingWentWrongFragment()
                supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(this.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()
            }else{
                val item = response.body()
                CachedData.platforms.addAll(item!!)
                loadingFragment.setProgress(100)
                val fragment = MyMainFragment()
                supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(this.getString(R.string.main_fragment)).commitAllowingStateLoss()

            }


        }
        apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

    }

    private fun loadData() {

        val map = mutableMapOf<String,String>()
        map["filter[rating][gte]"] = "90"
        dataService.service.getGames(map).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                val fragment = MySomethingWentWrongFragment()
                supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(this.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()
            }else{
                val item = response.body()
                CachedData.games.addAll(item!!)
                val genres : MutableList<Int> = mutableListOf()
                val platforms : MutableList<Int> = mutableListOf()
                item.forEach{
                    if(it.genres != null) {
                        genres.addAll(it.genres!!)
                    }
                    if(it.platforms != null) {
                        platforms.addAll(it.platforms!!)
                    }

                }


                genreString = createIDString(genres)
                platformString = createIDString(platforms)
                loadingFragment.setProgress(25)
                getAllGenres()
                getAllPlatforms()
            }
        }
        apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
    }

    private fun getAllGenres() {
        dataService.service.getAllGenres(genreString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                val fragment = MySomethingWentWrongFragment()
                supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack(this.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()
            } else {
                val item = response.body()
                item?.add(Genre(11,this.getString(R.string.real_time_strategy)))
                CachedData.genres.addAll(item!!)
                loadingFragment.setProgress(66)
            }
        }
        apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

    }


}



