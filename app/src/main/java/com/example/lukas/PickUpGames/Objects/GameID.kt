package com.example.lukas.PickUpGames.Objects

data class GameID(
        var game: Int,
        var date : Long?
)