package com.example.lukas.PickUpGames.Objects

data class Platform(
        var name : String,
        var id : Int,
        var logo : ImageClass?


) {
    override fun toString(): String {
        return name
    }
}

