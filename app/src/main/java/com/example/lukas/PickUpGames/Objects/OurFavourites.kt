package com.example.lukas.PickUpGames.Objects

data class OurFavourites(
        var success : Boolean,
        var code : Int,
        var our_favorites : MutableList<GameID>?
)