package com.example.lukas.PickUpGames.Objects

data class PEGI(
        var rating : Int?,
        var synopsis : String?
)