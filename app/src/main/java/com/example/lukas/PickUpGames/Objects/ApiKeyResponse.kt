package com.example.lukas.PickUpGames.Objects

data class ApiKeyResponse(
        var success : Boolean,
        var code : Int,
        var apikey : String?,
        var message : String?
)