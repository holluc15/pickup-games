package com.example.lukas.PickUpGames.Adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Fragments.MyGameFragment
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.Objects.*
import com.example.lukas.PickUpGames.R
import com.example.lukas.PickUpGames.replaceFragmentSafely
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.game_item.view.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class MyGameAdapter(var myDataset: MutableList<Game>? = mutableListOf(), var context : Context?,var category: Int,var openedBy : String) : RecyclerView.Adapter<MyGameAdapter.ViewHolder>() {

    var adapter : MyMainAdapter ?= null
    var lastPosition : Int = -1
    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.game_item, parent, false) as RelativeLayout

        return ViewHolder(cont).listen { pos, type ->
            val item = myDataset?.get(pos)
            (context as MainActivity).replaceFragmentSafely(MyGameFragment.newInstance(item!!,getGradient(category),openedBy),context!!.getString(R.string.game_fragment),true,R.id.frame,android.R.anim.fade_in,android.R.anim.fade_out)

        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val stars = holder.layout.ratingbar.progressDrawable as LayerDrawable
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP)

        myDataset?.let {
            val item =  myDataset!![position]
            holder.layout.game_name.text = item.name
            val cover = item.cover?.url
            if(cover != null) {
                Picasso.get().load("http:$cover").resize(250,250).placeholder(R.drawable.ic_help_outline_black_24dp).into(holder.layout.game_icon, object : Callback {

                    override fun onSuccess() {
                        //do nothing
                    }

                    override fun onError(e: Exception?) {
                        println(e?.message)
                        Picasso.get().load("http:${setImageSize(cover)}").placeholder(R.drawable.ic_help_outline_black_24dp).error(R.drawable.ic_cancel).into(holder.layout.game_icon)
                    }

                })

            }



            if(item.genres != null) {
                val genre = getGenreOfId(item.genres!![0])
                    holder.layout.genre_text.text = genre.name
            }



            setGradient(holder,category)
            setAnimation(holder.itemView, position)



                if(item.rating != null) {
                    holder.layout.ratingbar.visibility = View.VISIBLE
                    holder.layout.rating_text.visibility = View.VISIBLE
                    holder.layout.release_date_layout.visibility = View.GONE
                    holder.layout.ratingbar.rating = getRating(item.rating!!.toFloat())

                    holder.layout.rating_text.text = (context?.getString(R.string.get_rating,getRating(item.rating!!.toFloat())))
                }else {

                    if(item.first_release_date != null) {
                        holder.layout.ratingbar.visibility = View.GONE
                        holder.layout.rating_text.visibility = View.GONE
                        holder.layout.release_date_layout.visibility = View.VISIBLE

                         val date = getDate((item.first_release_date)!!.toLong(),"dd. MMMM yyyy")
                         holder.layout.release_date.text = (context?.getString(R.string.release_date,date))

                    }


                }



            if(item.pegi != null) {
                when (item.pegi!!.rating) {
                    1 -> {
                        holder.layout.pegi_image.setImageResource(R.drawable.pegi_3)
                    }

                    2 -> {
                        holder.layout.pegi_image.setImageResource(R.drawable.pegi_7)
                    }

                    3 -> {
                        holder.layout.pegi_image.setImageResource(R.drawable.pegi_12)
                    }

                    4 -> {
                        holder.layout.pegi_image.setImageResource(R.drawable.pegi_16)
                    }

                    5 -> {
                        holder.layout.pegi_image.setImageResource(R.drawable.pegi_18)
                    }
                }
            }


        }
    }

    private fun getDate(milliSeconds: Long, dateFormat: String): String {
        val formatter = SimpleDateFormat(dateFormat,Locale.ENGLISH)

        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }

    private fun setGradient(holder: ViewHolder, category : Int) {
        when(category) {
            0-> {
                holder.layout.background_category.setBackgroundResource(R.drawable.first_item_gradient)
            }
            1 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.second_item_gradient)
            }
            2 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.third_item_gradient)
            }
            3 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.fourth_item_gradient)
            }
            4 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.fith_item_gradient)
            }
            5 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.sixth_item_gradient)
            }
            6 -> {
                holder.layout.background_category.setBackgroundResource(R.color.transparent_color)
            }
            7 -> {
                holder.layout.background_category.setBackgroundResource(R.color.transparent_color)
            }
            8 -> {
                holder.layout.background_category.setBackgroundResource(R.drawable.gradient_1)
            }

        }
    }

    private fun getGradient(category : Int) : Int {
        when(category) {
            0-> {
                return R.drawable.first_item_gradient
            }
            1 -> {
                return R.drawable.second_item_gradient
            }
            2 -> {
                return R.drawable.third_item_gradient
            }
            3 -> {
                return R.drawable.fourth_item_gradient
            }
            4 -> {
                return R.drawable.fith_item_gradient
            }
            5 -> {
                return R.drawable.sixth_item_gradient
            }
            6 -> {
                return R.color.transparent_color
            }
            7 -> {
                return R.drawable.gradient_1
            }
            8 -> {
                return R.drawable.gradient_1
            }
            
        }
        return 0
    }

    private fun getGenreOfId(id : Int) : Genre {
        val genres = CachedData.genres
        genres.forEach {
            if(it.id == id) {
                return it
            }
        }
        return Genre(0,(context!!.getString(R.string.not_found)))
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if(openedBy == "saved") {
            if (position > lastPosition) {
                val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
                viewToAnimate.startAnimation(animation)
                lastPosition = position
            }
        }else{
            if (position > lastPosition) {
                val animation = AnimationUtils.loadAnimation(context, R.anim.item_animation_fall_down)
                viewToAnimate.startAnimation(animation)
                lastPosition = position
            }
        }

    }

    private fun getRating(rating : Float) : Float {
        if(rating > 0 && rating <= 10f) {
            return 0.5f
        }
        if(rating > 10f && rating <= 20f) {
            return 1f
        }
        if(rating > 20f && rating <= 30f) {
            return 1.5f
        }
        if(rating > 30f && rating <= 40f) {
            return 2f
        }
        if(rating > 40f && rating <= 50f) {
            return 2.5f
        }
        if(rating > 50f && rating <= 60f) {
            return 3f
        }
        if(rating > 60f && rating <= 70f) {
            return 3.5f
        }
        if(rating > 70f && rating <= 80f) {
            return 4f
        }
        if(rating > 80f && rating <= 90f) {
            return 4.5f
        }
        if(rating > 90f && rating <= 100f) {
            return 5f
        }
        return 0f


    }


    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }

    private fun setImageSize(url : String) : String {
        return url.replace("t_thumb","cover_small")
    }


}
