package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.Adapter.MyFilterAdapter
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.R
import com.example.lukas.PickUpGames.Objects.Filter
import com.example.lukas.PickUpGames.replaceFragmentSafely
import kotlinx.android.synthetic.main.filter_activity.*


class MyFilterFragment : Fragment() {
    private var filterAdapter: MyFilterAdapter?= null
    private val options = mutableMapOf<String, String>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filter_activity,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        filterAdapter = MyFilterAdapter(context = context)


        filter_list.adapter = filterAdapter
        filter_list.layoutManager = LinearLayoutManager(context)
        addFilters()


        cancel_filter_button_filter.setOnClickListener {
            (context as MainActivity).supportFragmentManager.popBackStack()
        }

        apply_filter_button.setOnClickListener {

            if(filterChecked(filterAdapter!!)) {
                (context as MainActivity).replaceFragmentSafely(MyMainFragment.newInstance(this),context!!.getString(R.string.main_fragment),true,R.id.frame)
            }else{
                (context as MainActivity).supportFragmentManager.popBackStack()
            }

        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun filterChecked(adapter : MyFilterAdapter) : Boolean {
        val checkboxes = adapter.checkBoxes
        checkboxes.forEach {
            if(it.value) {
                return true
            }
        }
        return false
    }

    fun getOptionMap() : Map<String, String> {
        val checkBoxes = filterAdapter?.checkBoxes!!
        if(checkBoxes[0] == true) {
            val platform = filterAdapter?.selectedPlatform
            options["filter[platforms][eq]"] = "${platform?.id}"
            options["filter[rating][gte]"] = "40"
        }

        if(checkBoxes[1] == true) {
            val popularityMin = filterAdapter?.popularityMin
            val popularityMax = filterAdapter?.popularityMax
            if(popularityMin == popularityMax) {
                options["filter[popularity][eq]"] = "$popularityMax"
            }else{
                options["filter[popularity][lte]"] = "$popularityMax"
                options["filter[popularity][gte]"] = "$popularityMin"
            }

            options["filter[rating][gte]"] = "40"

        }
        if(checkBoxes[2] == true) {
            val rating = filterAdapter?.selectedRating
            var upperRating = 0
            var underRating = 0
            if(rating != null) {

                when(getRating(rating.toFloat())) {
                    0.5f -> {
                        upperRating = 10
                        underRating = 0
                    }

                    1f -> {
                        upperRating = 20
                        underRating = 11
                    }
                    1.5f -> {
                        upperRating = 30
                        underRating = 21
                    }

                    2f -> {
                        upperRating = 40
                        underRating = 31
                    }

                    2.5f -> {
                        upperRating = 50
                        underRating = 41
                    }

                    3f -> {
                        upperRating = 60
                        underRating = 51
                    }
                    3.5f -> {
                        upperRating = 70
                        underRating = 61
                    }

                    4f -> {
                        upperRating = 80
                        underRating = 71
                    }

                    4.5f -> {
                        upperRating = 90
                        underRating = 81
                    }

                    5f -> {
                        upperRating = 100
                        underRating = 91
                    }

                }


            options["filter[rating][lte]"] = "$upperRating"
            options["filter[rating][gte]"] = "$underRating"
            }
        }
        return options
    }

    private fun getRating(rating : Float) : Float {
        if(rating > 0 && rating <= 10f) {
            return 0.5f
        }
        if(rating > 10f && rating <= 20f) {
            return 1f
        }
        if(rating > 20f && rating <= 30f) {
            return 1.5f
        }
        if(rating > 30f && rating <= 40f) {
            return 2f
        }
        if(rating > 40f && rating <= 50f) {
            return 2.5f
        }
        if(rating > 50f && rating <= 60f) {
            return 3f
        }
        if(rating > 60f && rating <= 70f) {
            return 3.5f
        }
        if(rating > 70f && rating <= 80f) {
            return 4f
        }
        if(rating > 80f && rating <= 90f) {
            return 4.5f
        }
        if(rating > 90f && rating <= 100f) {
            return 5f
        }
        return 0f


    }




    private fun addFilters() {
        val list = mutableListOf<Filter>()
        for (i in 1..3) {

            list.add(Filter("Filter",false))
        }
        filterAdapter?.myDataset = list
        filterAdapter?.notifyDataSetChanged()
    }


}