package com.example.lukas.PickUpGames.Adapter

import  android.content.Context
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Objects.Category
import kotlinx.android.synthetic.main.list_item.view.*
import android.app.Activity
import android.support.v7.widget.LinearLayoutManager
import android.util.DisplayMetrics
import android.view.View
import com.example.lukas.PickUpGames.Objects.CachedData
import com.example.lukas.PickUpGames.Objects.Game
import android.view.animation.AnimationUtils
import android.widget.Toast
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Fragments.MyNoConnectionFragment
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MyMainAdapter(var myDataset: MutableList<Category>? = mutableListOf(), var context : Context?) : RecyclerView.Adapter<MyMainAdapter.ViewHolder>() {

    var adapter : MyGameAdapter ?= null
    var lastPosition : Int = -1
    var selectedPos : Int = -1
    private var apiService = APIKeyServiceSDK.service
    private var dataService = DataServiceSDK.service
    var categoryVisible = mutableMapOf<Int,Boolean>()
    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item, parent, false) as RelativeLayout

        return ViewHolder(cont).listen { pos, type ->
            val item = myDataset?.get(pos)
            val expanded = item?.expanded
            item?.expanded = expanded!!.not()
            selectedPos = pos
            notifyDataSetChanged()
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val displaymetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displaymetrics)
        val deviceheight = displaymetrics.heightPixels / 6

        holder.layout.card.layoutParams.height = deviceheight

        myDataset?.let {
            val item = myDataset!![position]

            if(item.expanded && position == selectedPos) {
                adapter = MyGameAdapter(context = context,category = position,openedBy = "main")
                holder.layout.game_list.adapter = adapter

                holder.layout.game_list.layoutManager = LinearLayoutManager(context)

                if((context as MainActivity).isNetworkAvailable()) {
                    addGames(holder,position)
                }else{
                    (context as MainActivity).replaceFragmentSafely(MyNoConnectionFragment(),context!!.getString(R.string.no_connection_fragment),true,R.id.frame)
                }

                holder.layout.expand_item.visibility = View.VISIBLE
            }else{
                holder.layout.expand_item.visibility = View.GONE
            }



            var color = 0
            when(position) {
                0 -> {
                    color = ResourcesCompat.getColor(context!!.resources,R.color.first_item,null)
                    holder.layout.headline_text.text = (context?.getString(R.string.top_games))
                    holder.layout.subtitle_text.text = (context?.getString(R.string.top_games_desc))
                    holder.layout.image.setBackgroundResource(R.drawable.ic_trending_up_black_24dp)
                }
                1 -> {
                    color = ResourcesCompat.getColor(context!!.resources,R.color.second_item,null)
                    holder.layout.headline_text.text = (context?.getString(R.string.coming_soon))
                    holder.layout.subtitle_text.text = (context?.getString(R.string.coming_soon_desc))
                    holder.layout.image.setBackgroundResource(R.drawable.ic_fiber_new_black_24dp)

                }
                2 -> {
                    color = ResourcesCompat.getColor(context!!.resources,R.color.third_item,null)
                    holder.layout.headline_text.text = (context?.getString(R.string.recommended))
                    holder.layout.subtitle_text.text = (context?.getString(R.string.recommended_desc))
                    holder.layout.image.setBackgroundResource(R.drawable.ic_short_text_black_24dp)
                }
                3 -> {
                    color = ResourcesCompat.getColor(context!!.resources,R.color.fourth_item,null)
                    holder.layout.headline_text.text = (context?.getString(R.string.best_rating))
                    holder.layout.subtitle_text.text = (context?.getString(R.string.best_rating_desc))
                    holder.layout.image.setBackgroundResource(R.drawable.ic_grade_black_24dp)
                }
                4 -> {
                    color = ResourcesCompat.getColor(context!!.resources,R.color.sixth_item,null)
                    holder.layout.headline_text.text = (context?.getString(R.string.our_favourites))
                    holder.layout.subtitle_text.text = (context?.getString(R.string.our_favourites_desc))
                    holder.layout.image.setBackgroundResource(R.drawable.ic_favorite_black_24dp)
                }
            }
            holder.layout.card.setCardBackgroundColor(color)
            setGradient(holder,position)
            item.itemColor = color

            setAnimation(holder.itemView, position)

        }
    }

    private fun setGradient(holder: ViewHolder, category : Int) {
        when(category) {
            0-> {
                holder.layout.game_list.setBackgroundResource(R.drawable.first_item_gradient)
            }
            1 -> {
                holder.layout.game_list.setBackgroundResource(R.drawable.second_item_gradient)
            }
            2 -> {
                holder.layout.game_list.setBackgroundResource(R.drawable.third_item_gradient)
            }
            3 -> {
                holder.layout.game_list.setBackgroundResource(R.drawable.fourth_item_gradient)
            }
            4 -> {
                holder.layout.game_list.setBackgroundResource(R.drawable.fith_item_gradient)
            }

        }
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }

    private fun addGames(holder : ViewHolder,category: Int) {
        val list = mutableListOf<Game>()
        when(category) {
            0 -> {
                if(CachedData.topgames.size == 0) {
                    holder.layout.loading_card_layout.visibility = View.VISIBLE
                    apiService.getTopGames().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if(it.isSuccessful) {
                            val items = it.body()
                            val topGames = items!!.top_games
                            val ids = mutableListOf<Int>()
                            if(topGames != null) {
                                if(topGames.size > 0) {
                                    topGames.forEach {
                                        ids.add(it.game_id)
                                    }
                                    val idString = (context as MainActivity).createIDString(ids)
                                    dataService.getGamesById(idString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                                        if(it.isSuccessful) {
                                            val games = it.body()
                                            list.addAll(games!!)
                                            CachedData.topgames = list
                                            adapter?.myDataset = list
                                            adapter?.notifyDataSetChanged()
                                            holder.layout.loading_card_layout.visibility = View.GONE
                                        }
                                    }
                                    apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
                                }
                            }else{
                                holder.layout.loading_card_layout.visibility = View.GONE
                                Toast.makeText(context,(context?.getString(R.string.no_games_to_view)),Toast.LENGTH_LONG).show()
                            }



                        }
                    }

                }else{
                    list.addAll(CachedData.topgames)
                }

            }
            1 -> {
                if(CachedData.comingsoon.size == 0) {
                    holder.layout.loading_card_layout.visibility = View.VISIBLE
                    dataService.getComingSoonGames(System.currentTimeMillis(),"*").subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if(it.isSuccessful) {
                            val items = it.body()
                            val ids = mutableListOf<Int>()
                            items?.forEach {
                                ids.add(it.game)
                            }

                            val idString = (context as MainActivity).createIDString(ids)
                            dataService.getGamesById(idString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                                if(it.isSuccessful) {
                                    var games = it.body()
                                    if (games != null) {
                                        games.forEach {
                                            println(it.name)
                                            if(it.first_release_date != null) {
                                                if(it.cover != null && it.rating == null && it.first_release_date!! > System.currentTimeMillis()) {
                                                    if(!list.contains(it)) {
                                                        list.add(it)
                                                    }
                                                }
                                            }

                                        }

                                    }
                                    CachedData.comingsoon = list
                                    adapter?.myDataset = list
                                    adapter?.notifyDataSetChanged()
                                    holder.layout.loading_card_layout.visibility = View.GONE

                                }


                            }
                            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

                        }else{
                            Toast.makeText(context,(context?.getString(R.string.something_went_wrong)),Toast.LENGTH_LONG).show()
                        }
                    }
                    apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}


                }else{
                    list.addAll(CachedData.comingsoon)
                }

            }
            2 -> {
                val games = CachedData.games
                games.forEach {
                    if(it.rating != null) {
                        if(it.rating!! >= 75 && it.cover != null && it.popularity != null &&
                                it.platforms != null && it.genres != null && it.websites != null
                                && it.summary != null && it.screenshots != null && it.videos != null) {
                            list.add(it)
                        }
                    }

                }

            }
            3 -> {
                val games = CachedData.games
                games.forEach {
                    if(it.rating != null) {
                        if(it.cover != null && it.rating!! >= 95) {
                            list.add(it)
                        }
                    }
                }
                categoryVisible[category] = false
            }
            4 -> {
                if(CachedData.ourFavourites.size == 0) {
                    holder.layout.loading_card_layout.visibility = View.VISIBLE
                    apiService.getOurFavourites().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if(it.isSuccessful) {
                            val item = it.body()
                            println(item?.our_favorites)
                            val gameIds = mutableListOf<Int>()
                            item?.our_favorites?.forEach {

                                gameIds.add(it.game)
                            }

                            val idString = (context as MainActivity).createIDString(gameIds)
                            if(idString.isNotEmpty()) {
                                dataService.getGamesById(idString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                                    if(it.isSuccessful) {
                                        val games = it.body()
                                        CachedData.ourFavourites = games!!
                                        adapter?.myDataset = games
                                        adapter?.notifyDataSetChanged()
                                        holder.layout.loading_card_layout.visibility = View.GONE
                                    }
                                }
                                apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
                            }

                        }
                    }
                }else{
                    list.addAll(CachedData.ourFavourites)
                }
            }
        }


        adapter?.myDataset = list
        adapter?.notifyDataSetChanged()
    }

}



