package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Adapter.MyGameAdapter
import com.example.lukas.PickUpGames.Objects.CachedData
import com.example.lukas.PickUpGames.Objects.Game
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.search_screen.*


class MySearchFragment : Fragment() {


    private var service = DataServiceSDK.service
    private lateinit var adapter : MyGameAdapter
    private var genreString = ""
    private var platformString = ""
    private var apiService = APIKeyServiceSDK.service
    private var filteredGames = mutableListOf<Game>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.search_screen,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        search_view.onActionViewExpanded()
        search_result_list.layoutManager = LinearLayoutManager(context)
        adapter = MyGameAdapter(context = context,category = 7,openedBy = "search")
        search_result_list.adapter = adapter



        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // do something on text submit
                if((context as MainActivity).isNetworkAvailable()) {
                    val map = mutableMapOf<String, String>()
                    map["search"] = query
                    //map["order"] = "popularity:desc"
                    service.searchForGame(map).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->

                        if (!response.isSuccessful) {
                            search_instruction_layout.visibility = View.VISIBLE
                            status_text.text = context?.getString(R.string.search_instructions)
                            status_image.setImageResource(R.drawable.ic_present_to_all_black_24dp)
                            (context as MainActivity).supportFragmentManager.popBackStack()
                            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,MySomethingWentWrongFragment()).addToBackStack(context!!.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()

                        }else{
                            search_instruction_layout.visibility = View.GONE
                            val items = response.body()
                            if(items?.size!! > 0) {


                            val all = CachedData.games
                            filteredGames = filterGames(items)

                            all.addAll(filteredGames)
                            CachedData.games = all




                            val genres : MutableList<Int> = mutableListOf()
                            val platforms : MutableList<Int> = mutableListOf()

                            filteredGames.forEach{
                                if(it.genres != null) {
                                    genres.addAll(it.genres!!)
                                }
                                if(it.platforms != null) {
                                    platforms.addAll(it.platforms!!)
                                }

                            }


                            genreString = (context as MainActivity).createIDString(genres)
                            platformString = (context as MainActivity).createIDString(platforms)
                            getAllPlatforms()
                            }else{
                                adapter.myDataset = null
                                adapter.notifyDataSetChanged()
                                search_instruction_layout.visibility = View.VISIBLE
                                status_text.text = context?.getString(R.string.nothing_found)
                                status_image.setImageResource(R.drawable.ic_cancel)
                            }
                        }
                    }
                    apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

                }else{
                    val fragment = MyNoConnectionFragment()
                    (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(context!!.getString(R.string.no_connection_fragment)).commitAllowingStateLoss()
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // do something when text changes
                return false
            }
        })

        main_menu_button_fav.setOnClickListener {
            (context as MainActivity).replaceFragmentSafely(MyMainFragment(),context!!.getString(R.string.main_fragment),true,R.id.frame)
        }

        super.onViewCreated(view, savedInstanceState)
    }


    private fun filterGames(items : MutableList<Game>) : MutableList<Game> {
        val filteredGames = mutableListOf<Game>()

        items.forEach {
            //filter
            if(it.rating != null && it.cover != null) {
                filteredGames.add(it)
            }
        }
        return filteredGames
    }

    private fun getAllGenres() {
        if(genreString.isNotEmpty()) {

        service.getAllGenres(genreString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                (context as MainActivity).replaceFragmentSafely(MySomethingWentWrongFragment(),context!!.getString(R.string.something_went_wrong_fragment),true,R.id.frame)
            }else{
                val item = response.body()
                val all = CachedData.genres
                all.addAll(item!!)
                CachedData.genres.addAll(all)
                adapter.myDataset = filteredGames
                adapter.notifyDataSetChanged()
            }
        }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

        }else{
            adapter.myDataset = null
            adapter.notifyDataSetChanged()
            search_instruction_layout.visibility = View.VISIBLE
            status_text.text = context?.getString(R.string.nothing_found)
            status_image.setImageResource(R.drawable.ic_cancel)
        }
    }

    private fun getAllPlatforms() {
        if(platformString.isNotEmpty()) {

        service.getAllPlatforms(platformString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
            if (!response.isSuccessful) {
                (context as MainActivity).replaceFragmentSafely(MySomethingWentWrongFragment(),context!!.getString(R.string.something_went_wrong_fragment),true,R.id.frame)
            }else{
                val item = response.body()
                val all = CachedData.platforms
                all.addAll(item!!)
                CachedData.platforms.addAll(all)
                getAllGenres()

            }
        }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

        }else{
            adapter.myDataset = null
            adapter.notifyDataSetChanged()
            search_instruction_layout.visibility = View.VISIBLE
            status_text.text = context?.getString(R.string.nothing_found)
            status_image.setImageResource(R.drawable.ic_cancel)
        }
    }

}