package com.example.lukas.PickUpGames.Fragments

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.Toast
import com.example.lukas.PickUpGames.Adapter.MyPlatformAdapter
import kotlinx.android.synthetic.main.game_activity.*
import android.graphics.PorterDuff
import android.graphics.drawable.LayerDrawable
import android.preference.PreferenceManager
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import com.example.lukas.PickUpGames.Adapter.MyScreenshotAdapter
import com.example.lukas.PickUpGames.Objects.*
import com.squareup.picasso.Picasso
import android.view.LayoutInflater
import android.webkit.WebChromeClient
import android.webkit.WebSettings.PluginState
import android.webkit.WebViewClient
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Adapter.MySimilarGameAdapter
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class MyGameFragment : Fragment() {

    private var reduceRequests = true

    private var game : Game ?= null
    private var platformAdapter : MyPlatformAdapter ?= null
    private var screenshotAdapter : MyScreenshotAdapter ?= null
    private var gradient : Int = 0
    private lateinit var preferences: SharedPreferences
    private var sharedKey: String = ""
    private var sharedLikes: String = ""
    private var similarGameAdapter : MySimilarGameAdapter ?= null
    private var dataService = DataServiceSDK.service
    private var gameString = ""
    private var searchGameIds = mutableListOf<Int>()
    private var openedBy : String = ""
    private var platformString : String =""
    private var platformIds = mutableListOf<Int>()
    private var apiService = APIKeyServiceSDK.service


    companion object {
        fun newInstance(game: Game, gradient : Int,openedBy : String) = MyGameFragment().apply {
            this.game = game
            this.gradient = gradient
            this.openedBy = openedBy
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.game_activity,container,false)

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if((context as MainActivity).isNetworkAvailable()) {
            sharedKey = (context!!.getString(R.string.saved_games_preference))
            sharedLikes = (context!!.getString(R.string.liked_games_preference))
            preferences = PreferenceManager.getDefaultSharedPreferences(context)


            val stars = rating_bar.progressDrawable as LayerDrawable
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP)

            game_name.text = game?.name

            if (game?.genres != null) {
                val genre = getGenreOfId(game?.genres!![0])
                game_genre.text = genre.name
            }


            var savedGameIDs = preferences.getStringSet(sharedKey, emptySet())
            val toast = Toast.makeText(context, "", Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.BOTTOM, 0, 20)
            save_game_button.setOnClickListener {
                savedGameIDs = preferences.getStringSet(sharedKey, emptySet())
                val set = mutableSetOf<String>()
                val id = game?.id.toString()

                set.addAll(savedGameIDs)
                if (set.contains(id)) {
                    set.addAll(savedGameIDs)
                    set.remove(id)
                    preferences.edit().putStringSet(sharedKey, set).apply()
                    save_game_button.setImageResource(R.drawable.ic_bookmark_border_black_24dp)
                    toast.setText((context?.getString(R.string.removed_game_saved)))

                } else {
                    set.addAll(savedGameIDs)
                    set.add(id)
                    preferences.edit().putStringSet(sharedKey, set).apply()
                    toast.setText((context?.getString(R.string.added_game_saved)))
                    save_game_button.setImageResource(R.drawable.ic_bookmark_black_24dp)
                }
                toast.show()

            }

            if (savedGameIDs.contains(game?.id.toString())) {
                save_game_button.setImageResource(R.drawable.ic_bookmark_black_24dp)

            } else {
                save_game_button.setImageResource(R.drawable.ic_bookmark_border_black_24dp)
            }


            platformAdapter = MyPlatformAdapter(context = context)
            screenshotAdapter = MyScreenshotAdapter(context = context)
            similarGameAdapter = MySimilarGameAdapter(context = context, openedBy = "similar_game")

            val platformIDs = game?.platforms

            if (platformIDs != null) {
                platforms_card.visibility = View.VISIBLE
                val gamePlatforms = getAllPlatformsFromGame(platformIDs)
                if (platformIds.size > 0) {
                    platformString = (context as MainActivity).createIDString(platformIds)
                    if (platformString.isNotEmpty()) {
                        if ((context as MainActivity).isNetworkAvailable()) {
                            dataService.getAllPlatforms(platformString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
                                if (response.isSuccessful) {
                                    val items = response.body()
                                    gamePlatforms.addAll(items!!)
                                    platformAdapter?.myDataset = gamePlatforms
                                    game_platforms.adapter = platformAdapter
                                    platformAdapter?.notifyDataSetChanged()
                                    StartSnapHelper().attachToRecyclerView(game_platforms)
                                    game_platforms.layoutManager = LinearLayoutManager(context)
                                    platform_layout.setBackgroundResource(gradient)
                                } else {
                                    (context as MainActivity).supportFragmentManager.popBackStack()
                                    (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, MySomethingWentWrongFragment()).addToBackStack(context!!.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()
                                }
                            }
                            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
                        } else {
                            val fragment = MyNoConnectionFragment()
                            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack(context!!.getString(R.string.no_connection_fragment)).commitAllowingStateLoss()
                        }

                    }

                } else {
                    platformAdapter?.myDataset = gamePlatforms
                    game_platforms.adapter = platformAdapter
                    platformAdapter?.notifyDataSetChanged()
                    StartSnapHelper().attachToRecyclerView(game_platforms)
                    game_platforms.layoutManager = LinearLayoutManager(context)
                    platform_layout.setBackgroundResource(gradient)
                }


            } else {
                platforms_card.visibility = View.GONE
            }


            val screenshots = game?.screenshots
            if (screenshots != null) {
                screenshot_card.visibility = View.VISIBLE
                screenshotAdapter?.myDataset = screenshots
                screenshots_list.adapter = screenshotAdapter
                screenshotAdapter?.notifyDataSetChanged()
                StartSnapHelper().attachToRecyclerView(screenshots_list)
                screenshots_list.layoutManager = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)
                screenshot_layout.setBackgroundResource(gradient)

            } else {
                screenshot_card.visibility = View.GONE
            }

            if (game?.external != null) {
                steam_card.visibility = View.VISIBLE
                view_onSteam_button.setOnClickListener {
                    val fragment = MyWebFragment.newInstance(game?.external!!.steam, true)
                    (context as MainActivity).replaceFragmentSafely(fragment, "WebFragment", true, R.id.frame)
                }
            } else {
                steam_card.visibility = View.GONE
            }



            if (game?.summary != null) {
                summary_card.visibility = View.VISIBLE
                summary_textView.text = game?.summary
            } else {
                summary_card.visibility = View.GONE
            }


            val cover = game?.cover?.url
            Picasso.get().load("http:$cover").placeholder(R.drawable.ic_help_outline_black_24dp).error(R.drawable.ic_cancel).into(game_icon_image)


            val rating = game?.total_rating
            if (rating != null) {
                rating_bar.progress = rating.toInt()
                rating_bar_text.text = context!!.getString(R.string.rating_total,rating.toInt())
            } else {
                rating_bar.progress = 0
                rating_bar_text.text = context!!.getString(R.string.n_a)
            }

            var likedGameIDs = preferences.getStringSet(sharedLikes, emptySet())
            toast.setGravity(Gravity.BOTTOM, 0, 20)
            favour_game_button.setOnClickListener {
                likedGameIDs = preferences.getStringSet(sharedLikes, emptySet())
                val set = mutableSetOf<String>()
                val id = game?.id.toString()

                set.addAll(likedGameIDs)
                if (set.contains(id)) {
                    set.addAll(likedGameIDs)
                    set.remove(id)
                    preferences.edit().putStringSet(sharedLikes, set).apply()
                    favour_game_button.setImageResource(R.drawable.ic_favorite_border_black_24dp)
                    toast.setText(context!!.getString(R.string.removed_game_liked))
                    apiService.updateLikes(game_id = game!!.id, dislike = true).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}

                } else {
                    set.addAll(likedGameIDs)
                    set.add(id)
                    preferences.edit().putStringSet(sharedLikes, set).apply()
                    toast.setText(context!!.getString(R.string.added_game_liked))
                    favour_game_button.setImageResource(R.drawable.ic_favorite_red)
                    apiService.updateLikes(game_id = game!!.id, dislike = false).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
                }
                toast.show()


            }

            if (likedGameIDs.contains(game?.id.toString())) {
                favour_game_button.setImageResource(R.drawable.ic_favorite_red)

            } else {
                favour_game_button.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            }

            if (game?.websites != null) {
                val websites = game?.websites!!
                if (websites.size > 0) {
                    website_button.setOnClickListener {
                        var official = false
                        websites.forEach {

                            if (it.category == 1) {
                                official = true
                                val fragment = MyWebFragment.newInstance(it.url, false)
                                (context as MainActivity).replaceFragmentSafely(fragment, "WebFragment", true, R.id.frame)
                            }

                        }

                        if (!official) {
                            val fragment = MyWebFragment.newInstance(game?.websites!![0].url, false)
                            (context as MainActivity).replaceFragmentSafely(fragment, "WebFragment", true, R.id.frame)
                        }

                    }


                }

            } else {
                if (game?.url != null) {
                    website_button.setOnClickListener {
                        val fragment = MyWebFragment.newInstance(game?.url!!, false)
                        (context as MainActivity).replaceFragmentSafely(fragment, "WebFragment", true, R.id.frame)
                    }

                } else {
                    website_button.setBackgroundResource(R.drawable.round_button_not)
                }
            }


            root_layout.setBackgroundResource(gradient)
            summary_layout.setBackgroundResource(gradient)
            rating_layout.setBackgroundResource(gradient)
            pegi_layout.setBackgroundResource(gradient)
            popularity_layout.setBackgroundResource(gradient)
            status_layout.setBackgroundResource(gradient)
            steam_layout.setBackgroundResource(gradient)
            similar_game_layout.setBackgroundResource(gradient)


            if (game?.rating != null && game?.total_rating != null) {
                user_rating_layout.visibility = View.VISIBLE
                val ratingUsers = rating_bar_users.progressDrawable as LayerDrawable
                ratingUsers.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP)
                rating_bar_users.rating = getRating(game?.rating!!.toFloat())
                val rating_count = game?.total_rating_count
                if (rating_count == 0) {
                    user_rating_count.text = "n/a"
                } else {
                    user_rating_count.text = context!!.getString(R.string.total_rating,rating_count)
                }

            } else {
                user_rating_layout.visibility = View.GONE
            }

            if (game?.aggregated_rating != null && game?.aggregated_rating_count != null) {
                external_rating_layout.visibility = View.VISIBLE
                val ratingExternal = rating_bar_external.progressDrawable as LayerDrawable
                ratingExternal.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP)
                rating_bar_external.rating = getRating(game?.aggregated_rating!!.toFloat())
                val rating_count = game?.aggregated_rating_count
                if (rating_count == 0) {
                    external_rating_count.text = "n/a"
                } else {
                    external_rating_count.text = context!!.getString(R.string.total_rating,rating_count)
                }

            } else {
                external_rating_layout.visibility = View.GONE
            }

            if (game?.videos != null) {
                trailer_card.visibility = View.VISIBLE
                val videoId = game?.videos!![0].video_id //change to trailer!
                video_frame.settings.javaScriptEnabled = true
                video_frame.settings.pluginState = PluginState.ON
                view_onYoutube_button.setOnClickListener {
                    chrome_webview.settings.javaScriptEnabled = true
                    chrome_webview.settings.pluginState = PluginState.ON

                    chrome_webview.loadUrl("http://www.youtube.com/embed/$videoId?autoplay=1")
                    chrome_webview.webChromeClient = WebChromeClient()

                }
                video_frame.loadUrl("http://www.youtube.com/embed/$videoId?autoplay=1")
                video_frame.webViewClient = WebViewClient()

            } else {
                trailer_card.visibility = View.GONE
            }

            if (game?.total_rating != null && game?.total_rating_count != null) {
                total_rating_layout.visibility = View.VISIBLE
                val ratingBoth = rating_bar_both.progressDrawable as LayerDrawable
                ratingBoth.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP)
                rating_bar_both.rating = getRating(game?.total_rating!!.toFloat())
                val rating_count = game?.total_rating_count
                if (rating_count == 0) {
                    total_rating_count.text = context!!.getString(R.string.n_a)
                } else {
                    total_rating_count.text = context!!.getString(R.string.total_rating,rating_count)
                }

            } else {
                total_rating_layout.visibility = View.GONE
            }

            if (user_rating_layout.visibility == View.GONE && external_rating_layout.visibility == View.GONE && total_rating_layout.visibility == View.GONE) {
                rating_card.visibility = View.GONE
            } else {
                rating_card.visibility = View.VISIBLE
            }

            game?.rating.let {
                if (it != null) {
                    rating_bar.rating = getRating(it.toFloat())

                    rating_bar_text.text = context!!.getString(R.string.get_rating,getRating(it.toFloat()))
                } else {
                    rating_bar.rating = 0f
                    rating_bar_text.text = context!!.getString(R.string.n_a)
                }
            }

            if (game?.pegi != null) {
                pegi_card.visibility = View.VISIBLE
                pegi_image.setImageResource(getPEGIImage(game?.pegi?.rating!!))
                val synopsis = game?.pegi?.synopsis
                if (synopsis != null) {
                    syopsis_text.visibility = View.VISIBLE
                    syopsis_text.text = synopsis
                } else {
                    syopsis_text.visibility = View.GONE
                }


            } else {
                pegi_card.visibility = View.GONE
            }

            if (game?.popularity != null) {
                val popularity = game?.popularity!!.toFloat()
                popularity_card.visibility = View.VISIBLE
                popularity_slider.progress = popularity
                popularity_slider.isEnabled = false
                popularity_progress_text.text = context!!.getString(R.string.popularity_total,popularity.toInt())

                when (popularity) {
                    in 0.0..33.0 -> {
                        popularity_name_text.text = context!!.getString(R.string.unpopular)
                        popularity_slider.circleProgressColor = Color.RED
                    }
                    in 33.1..66.0 -> {
                        popularity_name_text.text = context!!.getString(R.string.quite_popular)
                        popularity_slider.circleProgressColor = ResourcesCompat.getColor(context!!.resources, R.color.orange_color, null)
                    }
                    in 67.1..100.0 -> {
                        popularity_name_text.text = context!!.getString(R.string.really_popular)
                        popularity_slider.circleProgressColor = Color.GREEN
                    }
                    in 100.1..Double.MAX_VALUE -> {
                        popularity_name_text.text = context!!.getString(R.string.really_popular)
                        popularity_slider.circleProgressColor = Color.GREEN
                        popularity_slider.progress = 100f
                    }

                }


            } else {
                popularity_card.visibility = View.GONE
            }

            if (game?.status != null) {
                status_card.visibility = View.VISIBLE
                val status = game?.status
                when (status) {
                    0 -> {
                        status_text.text = context!!.getString(R.string.released)
                    }
                    2 -> {
                        status_text.text = context!!.getString(R.string.alpha)
                    }
                    3 -> {
                        status_text.text = context!!.getString(R.string.beta)
                    }
                    4 -> {
                        status_text.text = context!!.getString(R.string.early_access)
                    }
                    5 -> {
                        status_text.text = context!!.getString(R.string.offline)
                    }
                    6 -> {
                        status_text.text = context!!.getString(R.string.cancelled)
                    }
                }
            } else {
                status_card.visibility = View.GONE
            }



            main_menu_button.setOnClickListener {

            /*
             main,search,saved,similar
             */
                if (openedBy == "saved") {
                    (context as MainActivity).replaceFragmentSafely(MySavedGamesFragment(), context!!.getString(R.string.saved_games_fragment), false, R.id.frame)
                } else {
                    (context as MainActivity).supportFragmentManager.popBackStack()
                }
            }

            reduceRequests = CachedData.requestSaveMode

            if (game?.games != null && reduceRequests) {

                similar_games_card.visibility = View.VISIBLE

                similar_game_list.adapter = similarGameAdapter

                var dataSet = mutableListOf<Game>()
                dataSet = getGamesOfIds(game?.games!!)


                if (searchGameIds.size != 0) {
                    gameString = (context as MainActivity).createIDString(searchGameIds)
                    if ((context as MainActivity).isNetworkAvailable()) {
                        dataService.getGamesById(gameString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
                            if (response.isSuccessful) {
                                val items = response.body()
                                dataSet.addAll(items!!)
                                val filteredGames = mutableListOf<Game>()

                                //filter
                                dataSet.forEach {
                                    if (it.rating != null && it.cover != null && it.genres != null) {
                                        filteredGames.add(it)
                                    }
                                }

                                similarGameAdapter?.myDataset = filteredGames
                                similarGameAdapter?.notifyDataSetChanged()
                                StartSnapHelper().attachToRecyclerView(similar_game_list)
                                similar_game_list.layoutManager = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)

                            } else {
                                (context as MainActivity).supportFragmentManager.popBackStack()
                                (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, MySomethingWentWrongFragment()).addToBackStack(context!!.getString(R.string.something_went_wrong_fragment)).commitAllowingStateLoss()
                            }
                        }
                        apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
                    } else {
                        val fragment = MyNoConnectionFragment()
                        (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack(context!!.getString(R.string.no_connection_fragment)).commitAllowingStateLoss()
                    }
                }

            } else {
                similar_games_card.visibility = View.GONE
            }

        }else{
            val fragment = MyNoConnectionFragment()
            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack(context!!.getString(R.string.no_connection_fragment)).commitAllowingStateLoss()
        }




    super.onViewCreated(view, savedInstanceState)
}

    private fun getPEGIImage(rating : Int) : Int {

            when(rating) {
                1 -> {
                    return(R.drawable.pegi_3)
                }

                2 -> {
                    return(R.drawable.pegi_7)
                }

                3 -> {
                    return(R.drawable.pegi_12)
                }

                4 -> {
                    return(R.drawable.pegi_16)
                }

                5 -> {
                    return(R.drawable.pegi_18)
                }
            }
        return R.drawable.ic_cancel

    }

    private fun getGenreOfId(id : Int) : Genre {
        val genres = CachedData.genres
        genres.forEach {
            if(it.id == id) {
                return it
            }
        }
        return Genre(0,context!!.getString(R.string.not_found))
    }

    private fun getAllPlatformsFromGame(ids : MutableList<Int>) : MutableList<Platform> {
        val platforms = mutableListOf<Platform>()
        ids.forEach {
            var platform = getPlatformOfId(it)
            if(platform != null) {
                platforms.add(platform)
            }else{
                platformIds.add(it)
            }

        }
        return platforms
    }

    private fun getPlatformOfId(id : Int) : Platform? {
        val platforms = CachedData.platforms
        platforms.forEach {
            if(it.id == id) {
                return it
            }
        }
        return null
    }

    private fun getGamesOfIds(gameIds : MutableList<Int>) : MutableList<Game> {
        val games = mutableListOf<Game>()
        gameIds.forEach {
            val game = getGameOfId(it)
            if(game != null ) {
                games.add(game)
            }

        }

        val allGames = CachedData.games
        allGames.addAll(games)
        CachedData.games = allGames

        return games
    }


    private fun getGameOfId(id : Int) : Game? {
        val games = CachedData.games
        games.forEach {
            if(it.id == id) {
                return it
            }
        }

        searchGameIds.add(id)
        return null
    }

    private fun getRating(rating : Float) : Float {
        if(rating > 0 && rating <= 10f) {
            return 0.5f
        }
        if(rating > 10f && rating <= 20f) {
            return 1f
        }
        if(rating > 20f && rating <= 30f) {
            return 1.5f
        }
        if(rating > 30f && rating <= 40f) {
            return 2f
        }
        if(rating > 40f && rating <= 50f) {
            return 2.5f
        }
        if(rating > 50f && rating <= 60f) {
            return 3f
        }
        if(rating > 60f && rating <= 70f) {
            return 3.5f
        }
        if(rating > 70f && rating <= 80f) {
            return 4f
        }
        if(rating > 80f && rating <= 90f) {
            return 4.5f
        }
        if(rating > 90f && rating <= 100f) {
            return 5f
        }
        return 0f


    }

}