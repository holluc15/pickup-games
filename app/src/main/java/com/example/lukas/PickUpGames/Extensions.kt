package com.example.lukas.PickUpGames

import android.content.Context
import android.net.ConnectivityManager
import android.support.annotation.AnimRes
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

fun AppCompatActivity.replaceFragmentSafely(fragment: Fragment,
                                            tag: String,
                                            allowStateLoss: Boolean = false,
                                            @IdRes containerViewId: Int,
                                            @AnimRes enterAnimation: Int = 0,
                                            @AnimRes exitAnimation: Int = 0,
                                            @AnimRes popEnterAnimation: Int = 0,
                                            @AnimRes popExitAnimation: Int = 0) {
    val ft = supportFragmentManager
            .beginTransaction()
            .add(containerViewId, fragment)
            .addToBackStack(tag)
            .setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation)
    if (!supportFragmentManager.isStateSaved) {
        ft.commit()
    } else if (allowStateLoss) {
        ft.commitAllowingStateLoss()
    }
}

fun AppCompatActivity.createIDString(ids : MutableList<Int>) : String {

    var string = ""
    ids.forEach {
        string += "$it,"
    }
    if(string.isNotEmpty()) {
        string = string.substring(0,string.length-1)
    }
    return string
}

fun AppCompatActivity.isNetworkAvailable(): Boolean {
    val connectivityManager = (this as MainActivity).getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}