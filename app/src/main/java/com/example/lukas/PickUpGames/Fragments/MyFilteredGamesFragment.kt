package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.Adapter.MyGameAdapter
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.Objects.Game
import com.example.lukas.PickUpGames.R
import kotlinx.android.synthetic.main.filtered_games_layout.*

class MyFilteredGamesFragment : Fragment() {


    private var dataSet = mutableListOf<Game>()

    companion object {
        fun newInstance(dataSet : MutableList<Game>) = MyFilteredGamesFragment().apply {
            this.dataSet = dataSet
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filtered_games_layout,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        search_view.setOnClickListener {
            val fragment = MySearchFragment()
            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(context!!.getString(R.string.search_fragment)).commitAllowingStateLoss()
        }

        main_menu_button_filteredgames.setOnClickListener {
            val fragment = MyMainFragment()
            (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame,fragment).addToBackStack(context!!.getString(R.string.main_fragment)).commitAllowingStateLoss()
        }

        filter_result_list.layoutManager = LinearLayoutManager(context)
        val adapter = MyGameAdapter(context = context,category = 0,openedBy = "filter")
        adapter.myDataset = dataSet
        filter_result_list.adapter = adapter
        adapter.notifyDataSetChanged()

        if(dataSet.size > 0) {
            filter_instruction_layout.visibility = View.GONE
        }else{
            filter_instruction_layout.visibility = View.VISIBLE
        }

        super.onViewCreated(view, savedInstanceState)
    }
}