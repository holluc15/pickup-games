package com.example.lukas.PickUpGames.Objects

class CachedData {
    companion object {
        var platforms: MutableList<Platform> = mutableListOf()
        var games: MutableList<Game> = mutableListOf()
        var genres: MutableList<Genre> = mutableListOf()
        var requestSaveMode : Boolean = false
        var topgames : MutableList<Game> = mutableListOf()
        var comingsoon : MutableList<Game> = mutableListOf()
        var ourFavourites : MutableList<Game> = mutableListOf()
    }

}

