package com.example.lukas.PickUpGames.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Fragments.MyBigImageFragment
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.Objects.ImageClass
import com.example.lukas.PickUpGames.R
import com.example.lukas.PickUpGames.replaceFragmentSafely
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.screenshot_item.view.*
import java.lang.Exception


class MyScreenshotAdapter(var myDataset: MutableList<ImageClass>? = mutableListOf(), var context : Context?) : RecyclerView.Adapter<MyScreenshotAdapter.ViewHolder>() {

    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.screenshot_item, parent, false) as RelativeLayout

        return ViewHolder(cont).listen { pos, type ->
            val item = myDataset?.get(pos)

            val fragment = MyBigImageFragment.newInstance(item!!)
            (context as MainActivity).replaceFragmentSafely(fragment,context!!.getString(R.string.big_image_fragment),true,R.id.frame)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        myDataset?.let {
            val item =  myDataset!![position]

                val screenshot = item.url
                Picasso.get().load("http:${setImageSize(screenshot)}").placeholder(R.drawable.ic_help_outline_black_24dp).into(holder.layout.screenshot,object : Callback{
                    override fun onSuccess() {
                        holder.itemView.visibility = View.VISIBLE
                    }

                    override fun onError(e: Exception?) {
                      holder.itemView.visibility = View.GONE
                    }

                })



        }
    }

    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }

    private fun setImageSize(url : String) : String {
        return url.replace("t_thumb","t_720p")
    }



}