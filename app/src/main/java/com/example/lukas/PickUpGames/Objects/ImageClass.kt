package com.example.lukas.PickUpGames.Objects

data class ImageClass(
        var url : String,
        var width : Int,
        var height : Int
)