package com.example.lukas.PickUpGames.Adapter

import android.content.Context
import android.graphics.Color
import android.net.ConnectivityManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import com.example.lukas.PickUpGames.Objects.CachedData
import com.example.lukas.PickUpGames.Objects.Filter
import com.example.lukas.PickUpGames.Objects.Platform
import kotlinx.android.synthetic.main.filter_item.view.*
import android.support.v4.content.res.ResourcesCompat
import android.widget.SeekBar
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Fragments.MyNoConnectionFragment
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import me.tankery.lib.circularseekbar.CircularSeekBar


class MyFilterAdapter(var myDataset: MutableList<Filter>? = mutableListOf(), var context : Context?) : RecyclerView.Adapter<MyFilterAdapter.ViewHolder>() {

    private var popularPlatforms = arrayListOf(34,39,6,14,3,49,48,9,130,41,9,82,162,163,165)
    val checkBoxes = mutableMapOf<Int, Boolean>()
    var selectedPlatform : Platform ?= null
    var popularityMin : Int = 0
    var popularityMax : Int = 100
    var selectedRating : Int = 0
    private var apiService = APIKeyServiceSDK.service
    private var dataService = DataServiceSDK.service


    class ViewHolder(val layout: RelativeLayout) : RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val cont = LayoutInflater.from(parent.context)
                .inflate(R.layout.filter_item, parent, false) as RelativeLayout

        return ViewHolder(cont)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        myDataset?.let {
            val layout = holder.layout

            when(position) {
                0 -> {
                    layout.filter_name.text = (context?.getString(R.string.platform))
                    layout.filter_icon.setImageResource(R.drawable.ic_computer_black_24dp)
                }

                1 -> {
                    layout.filter_name.text = (context?.getString(R.string.popularity))
                    layout.filter_icon.setImageResource(R.drawable.ic_trending_up_black_24dp)
                }

                2 -> {
                    layout.filter_name.text = (context?.getString(R.string.rating))
                    layout.filter_icon.setImageResource(R.drawable.ic_star_black_24dp)
                }

            }
            layout.filter_item_layout.setOnClickListener {
                onItemClick(layout,position,false)
            }

            layout.filter_checkbox.setOnClickListener {
                onItemClick(layout,position,true)
            }

            val popularitySlider = layout.popularity_slider
            popularitySlider.setTickCount(100)
            popularitySlider.setTickHeight(0f)
            popularitySlider.setOnRangeBarChangeListener { rangeBar, left, right ->
                popularityMin = left
                layout.popularity_slider_min.progress = left.toFloat()
                layout.popularity_progress_min_text.text = "$left"
                layout.popularity_slider_max.progress = right.toFloat()
                layout.popularity_progress_max_text.text = "$right"
                popularityMax = right
            }
            popularitySlider.setBarColor(ResourcesCompat.getColor(context!!.resources,R.color.default_dark_background,null))
            popularitySlider.setConnectingLineColor(Color.CYAN)
            popularitySlider.setConnectingLineWeight(5f)
            popularitySlider.setThumbIndices(1,99)
            popularitySlider.setThumbRadius(7.5f)
            popularitySlider.setBarWeight(5f)

            //popularitySlider.setThumbImageNormal(ResourcesCompat.getDrawable(context!!.resources,R.drawable.ic_thumb,null))
            layout.rating_slider.thumb = ResourcesCompat.getDrawable(context!!.resources,R.drawable.ic_thumb,null)
            layout.filter_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    val platform = (layout.filter_spinner.selectedItem as Platform)
                    selectedPlatform = platform
                }

            }

            val aa = ArrayAdapter(context,android.R.layout.simple_spinner_item,getAllPlatforms())
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            layout.filter_spinner.adapter = aa
            aa.notifyDataSetChanged()

            layout.rating_slider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                    layout.rating_bar_filter.rating = getRating(p1.toFloat())
                    selectedRating = p1
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }

            })

            layout.popularity_slider_max.isEnabled = false
            layout.popularity_slider_max.circleProgressColor = Color.GREEN
            layout.popularity_slider_min.circleProgressColor = Color.RED
            layout.popularity_slider_max.setOnSeekBarChangeListener(object : CircularSeekBar.OnCircularSeekBarChangeListener {
                override fun onProgressChanged(circularSeekBar: CircularSeekBar?, progress: Float, fromUser: Boolean) {
                    if(progress > 0 && progress <= 33) {
                        layout.popularity_slider_max.circleProgressColor = Color.RED
                    }
                    if(progress > 33 && progress <= 66) {
                        layout.popularity_slider_max.circleProgressColor = ResourcesCompat.getColor(context!!.resources,R.color.orange_color,null)
                    }
                    if(progress > 66 && progress <= 100) {
                        layout.popularity_slider_max.circleProgressColor = Color.GREEN
                    }
                }

                override fun onStartTrackingTouch(seekBar: CircularSeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: CircularSeekBar?) {
                }


            })

            layout.popularity_slider_min.setOnSeekBarChangeListener(object : CircularSeekBar.OnCircularSeekBarChangeListener {
                override fun onProgressChanged(circularSeekBar: CircularSeekBar?, progress: Float, fromUser: Boolean) {
                    if(progress > 0 && progress <= 33) {
                        layout.popularity_slider_min.circleProgressColor = Color.RED
                    }
                    if(progress > 33 && progress <= 66) {
                        layout.popularity_slider_min.circleProgressColor = ResourcesCompat.getColor(context!!.resources,R.color.orange_color,null)
                    }
                    if(progress > 66 && progress <= 100) {
                        layout.popularity_slider_min.circleProgressColor = Color.GREEN
                    }
                }

                override fun onStartTrackingTouch(seekBar: CircularSeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: CircularSeekBar?) {
                }


            })
            layout.popularity_slider_min.isEnabled = false


        }

    }

    private fun getRating(rating : Float) : Float {
        if(rating > 0 && rating <= 10f) {
            return 0.5f
        }
        if(rating > 10f && rating <= 20f) {
            return 1f
        }
        if(rating > 20f && rating <= 30f) {
            return 1.5f
        }
        if(rating > 30f && rating <= 40f) {
            return 2f
        }
        if(rating > 40f && rating <= 50f) {
            return 2.5f
        }
        if(rating > 50f && rating <= 60f) {
            return 3f
        }
        if(rating > 60f && rating <= 70f) {
            return 3.5f
        }
        if(rating > 70f && rating <= 80f) {
            return 4f
        }
        if(rating > 80f && rating <= 90f) {
            return 4.5f
        }
        if(rating > 90f && rating <= 100f) {
            return 5f
        }
        return 0f


    }



    private fun onItemClick(layout : RelativeLayout,position: Int,isCheckBox : Boolean) {
        if(!isCheckBox) {
            layout.filter_checkbox.isChecked = layout.filter_checkbox.isChecked.not()
        }


        if(layout.filter_checkbox.isChecked){

            checkBoxes[position] = true
            when(position) {
                0 -> {
                    layout.spinner_layout.visibility = View.VISIBLE

                }

                1 -> {
                    layout.slider_layout_popularity.visibility = View.VISIBLE
                }

                2 -> {
                    layout.slider_layout_rating.visibility = View.VISIBLE
                }
            }

        }else{
            checkBoxes[position] = false
            when(position) {
                0 -> {
                    layout.spinner_layout.visibility = View.GONE
                }

                1 -> {
                    layout.slider_layout_popularity.visibility = View.GONE
                }

                2 -> {
                    layout.slider_layout_rating.visibility = View.GONE
                }
            }
        }
    }



    override fun getItemCount(): Int {
        return myDataset?.size ?: 0
    }

    private fun getAllPlatforms() : MutableList<Platform> {
        val items = mutableListOf<Platform>()
        val ids = mutableListOf<Int>()
        if((context as MainActivity).isNetworkAvailable()) {
            popularPlatforms.forEach{
                val platform = containsPlatform(it)
                if(platform != null) {
                    items.add(platform)
                }else{
                    ids.add(it)

                }
            }

            val idString = (context as MainActivity).createIDString(ids)

            dataService.getAllPlatforms(idString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {
                if(it.isSuccessful) {
                    items.addAll(it.body()!!)
                }
            }
            apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
        }else{
            (context as MainActivity).replaceFragmentSafely(MyNoConnectionFragment(),context!!.getString(R.string.no_connection_fragment),true,R.id.frame)
        }
        return items
    }

    private fun containsPlatform(id : Int) : Platform? {
        val allPlatforms = CachedData.platforms

        allPlatforms.forEach {
            if(it.id == id) {
                return it
            }
        }
        return null
    }




}