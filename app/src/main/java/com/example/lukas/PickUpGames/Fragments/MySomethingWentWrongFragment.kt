package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.R
import android.content.Intent
import kotlinx.android.synthetic.main.something_went_wrong_layout.*

class MySomethingWentWrongFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.something_went_wrong_layout,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        retry_button.setOnClickListener {
            val packageManager = context?.packageManager
            val intent = packageManager?.getLaunchIntentForPackage(context?.packageName)
            val componentName = intent!!.component
            val mainIntent = Intent.makeRestartActivityTask(componentName)
            context?.startActivity(mainIntent)
            System.exit(0)
        }
        super.onViewCreated(view, savedInstanceState)
    }

}