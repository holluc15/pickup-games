package com.example.lukas.PickUpGames.Objects

data class Filter (
        var name : String,
        var expanded : Boolean
)