package com.example.lukas.PickUpGames.Objects

data class TopGamesResult(
        var success : Boolean,
        var code : Int,
        var top_games : MutableList<LikedGames>?
)