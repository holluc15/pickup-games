package com.example.lukas.PickUpGames.Fragments

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.*
import com.example.lukas.PickUpGames.Adapter.MyGameAdapter
import com.example.lukas.PickUpGames.Objects.CachedData
import com.example.lukas.PickUpGames.Objects.Game
import com.example.lukas.PickUpGames.rest.APIKeyServiceSDK
import com.example.lukas.PickUpGames.rest.DataServiceSDK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.saved_games_acitivity.*

class MySavedGamesFragment : Fragment() {

    private lateinit var adapter : MyGameAdapter
    private lateinit var preferences: SharedPreferences
    private var sharedKey: String = ""
    private var dataService = DataServiceSDK.service
    private var apiService = APIKeyServiceSDK.service
    private var gameString : String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.saved_games_acitivity,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        preferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedKey = context!!.getString(R.string.saved_games_preference)

        adapter = MyGameAdapter(context = context,category = 7,openedBy = "saved")

        no_games_layout.visibility = View.GONE
        saved_loading_layout.visibility = View.VISIBLE
        saved_games_list.adapter = adapter

        saved_games_list.layoutManager = LinearLayoutManager(context)

        val savedGames = preferences.getStringSet(sharedKey, mutableSetOf())
        if(savedGames.isEmpty()) {
            no_games_layout.visibility = View.VISIBLE
        }else{
            no_games_layout.visibility = View.GONE
        }
        getGamesFromId(savedGames)

        main_menu_button_fav.setOnClickListener {
            (context as MainActivity).replaceFragmentSafely(MyMainFragment(),context!!.getString(R.string.main_fragment),true,R.id.frame)
        }
        search_button_fav.setOnClickListener {
            (context as MainActivity).replaceFragmentSafely(MySearchFragment(),context!!.getString(R.string.search_fragment),true,R.id.frame)
        }
        search_button_mid_fav.setOnClickListener {
            (context as MainActivity).replaceFragmentSafely(MySearchFragment(),context!!.getString(R.string.search_fragment),true,R.id.frame)
        }


        super.onViewCreated(view, savedInstanceState)
    }




    private fun getGamesFromId(ids : Set<String>) {
        val games = mutableListOf<Game>()


        val searchIds = mutableListOf<Int>()
        var searchBool = false
        ids.forEach {
            if(compareIds(it) != null) {
                val game = compareIds(it)
                    games.add(game!!)
                }else {
                searchBool = true
                searchIds.add(it.toInt())
            }
        }

        if(searchBool) {
            gameString = (context as MainActivity).createIDString(searchIds)
            if((context as MainActivity).isNetworkAvailable()) {
                dataService.getGamesById(gameString).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe { response ->
                    if (response.isSuccessful) {
                        val item = response.body()
                        games.addAll(item!!)
                        adapter.myDataset = games
                        adapter.notifyDataSetChanged()
                        saved_loading_layout.visibility = View.GONE
                        if(games.size == 0) {
                            no_games_layout.visibility = View.VISIBLE
                        }else{
                            no_games_layout.visibility = View.GONE
                        }
                    } else {
                        val fragment = MySomethingWentWrongFragment()
                        (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack("SomethingWentWrongFragment").commitAllowingStateLoss()
                    }

                }
                apiService.updateRequests(DataServiceSDK.apiKey).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe {}
            }else{
                val fragment = MySomethingWentWrongFragment()
                (context as MainActivity).supportFragmentManager.beginTransaction().add(R.id.frame, fragment).addToBackStack("SomethingWentWrongFragment").commitAllowingStateLoss()
            }
        }else{
            saved_loading_layout.visibility = View.GONE
        }


        adapter.myDataset = games

    }

    private fun compareIds(id : String) : Game? {
        val games = CachedData.games
        games.forEach {
            if(id.toInt().equals(it.id)) {
                return it
            }
        }
        return null
    }



}