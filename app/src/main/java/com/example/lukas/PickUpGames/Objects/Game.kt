package com.example.lukas.PickUpGames.Objects

data class Game(
        var id : Int,
        var url : String,
        var name : String,
        var genres : MutableList<Int>?,
        var cover : ImageClass?,
        var platforms : MutableList<Int>?,
        var popularity : Double?,
        var total_rating : Double?,
        var total_rating_count : Int?,
        var rating : Double?,
        var rating_count : Int?,
        var aggregated_rating : Double?,
        var aggregated_rating_count : Int?,
        var summary : String?,
        var storyline : String?,
        var pegi : PEGI?,
        var external : External?,
        var status : Int?,
        var screenshots : MutableList<ImageClass>?,
        var publishers : MutableList<Int>?,
        var websites : MutableList<Website>?,
        var videos : MutableList<Video>?,
        var games : MutableList<Int>?,
        var first_release_date : Long?
)