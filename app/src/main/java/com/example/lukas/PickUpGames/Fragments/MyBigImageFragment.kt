package com.example.lukas.PickUpGames.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.lukas.PickUpGames.MainActivity
import com.example.lukas.PickUpGames.Objects.ImageClass
import com.example.lukas.PickUpGames.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_big_layout.*


class MyBigImageFragment : Fragment() {

    private lateinit var image : ImageClass

    companion object {
        fun newInstance(image : ImageClass) = MyBigImageFragment().apply {
            this.image = image
        }

    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.image_big_layout,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Picasso.get().load("http:${setImageSize(image.url)}").error(R.drawable.ic_cancel).into(screenshot_big)

        root_layout_bigimage.setOnClickListener {
            (context as MainActivity).onBackPressed()
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun setImageSize(url : String) : String {
        return url.replace("t_thumb","t_720p")
    }



}