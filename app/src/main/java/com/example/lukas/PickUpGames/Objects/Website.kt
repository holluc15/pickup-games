package com.example.lukas.PickUpGames.Objects

data class Website(
        var url : String,
        var category: Int
)