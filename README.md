# PickUpGames

PickUpGames proviedes information about video games. It allows users to save their favourite game and it shows where the game can be downloaded. Furthermore, it also shows titles, which get released in the near future.

<img src="https://gitlab.com/holluc15/pickup-games/uploads/402de75b5f3f442989538d104c17bfed/Screenshot_2020-02-13_Store-Eintrag_-_PickUpGames_-_Google_Play_Console.png"/>

## Loading Screen
This Screen pops up at the startup of the application. It indicates the loading length of the app.<br>

<img src="https://gitlab.com/holluc15/pickup-games/uploads/9928f2bb7b89ca4464e19f9856566c87/WhatsApp_Image_2020-02-13_at_22.17.29_4_.jpeg" width=20%>

## MainMenu
This Fragment will pop up after the loading screen. It displays 5 categories of games the user can chose from.<br>

<img src="https://gitlab.com/holluc15/pickup-games/uploads/85eac64af698864c66701e2fd7b31629/WhatsApp_Image_2020-02-13_at_22.17.29_5_.jpeg" width=20%>

## MainMenu open
If a user clicks a item, a dropdown will pop up, where all games in this category are listed.<br>

<img src="https://gitlab.com/holluc15/pickup-games/uploads/ab7afd78b7756b1862b62a556070d873/WhatsApp_Image_2020-02-13_at_22.17.29_1_.jpeg" width=20%>

## GameMenu
The user is able to see a lot of details of a game if he clicks on it.<br>

<img src="https://gitlab.com/holluc15/pickup-games/uploads/bf29b0dfabd05c005c1a7e20532f8bf0/WhatsApp_Image_2020-02-13_at_22.17.29_3_.jpeg" width=20%>

## Search
It is also possible to search for a game using the build in search function.

<img src="https://gitlab.com/holluc15/pickup-games/uploads/606646f3f1c240a3f1e52180c1d00835/WhatsApp_Image_2020-02-13_at_22.17.29.jpeg" width=20%>
<br>

## Filter
 A filter can also be added if needed.<br>
 
<img src="https://gitlab.com/holluc15/pickup-games/uploads/c28cef3ad274a6528a47cfc1ca1b3dc7/WhatsApp_Image_2020-02-13_at_22.17.29_2_.jpeg" width=20%>



## Built With

* [Kotlin](https://kotlinlang.org/) - The programming language
* [Gradle](https://gradle.org/) - Dependency Management
* [NodeJS](https://nodejs.org/en/) - Used to create the API
* [GitLab](https://gitlab.com/) - Versioning software 


## Authors

* **Lukas Holzmann** - *Android Application*
* **Marco Haiden** - *Appilcation Programming Interface*
